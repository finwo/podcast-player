(function() {
  if(typeof ff === 'undefined') {
    throw new Error('ff framework not loaded!');
  }
  
  /**
   * ff( selector ).animate( parameter, target, duration )
   *     @param  {string} parameter
   *     @return {string} target
   *     @return {number} duration
   *
   *     Animate css values. Duration is in milliseconds and has a default of 400
   */
  
  ff.fn.animate     = function ( parameter, target, duration ) {
    var object = this;
    var supportedParameters = [
          'height', 'width',
          'marginTop',
          'marginRight',
          'marginBottom',
          'marginLeft',
          'paddingTop',
          'paddingRight',
          'paddingBottom',
          'paddingLeft'
        ],
        parameters          = parameter.forEach ? parameter : [ parameter ];
    parameters.forEach(function ( param ) {
      if ( supportedParameters.indexOf(param) < 0 ) return;
      duration = duration || 400;
      object.each(function () {
        var element       = this,
            elementTarget = target,
            elementOrigin = getComputedStyle(element)[ parameter ];
            
        function transitionEnd( event ) {
          if ( event.propertyName == parameter ) {
            element.style.transition   = '';
            if ( elementTarget == 'auto' ) element.style[ parameter ] = 'auto';
            element.removeEventListener('transitioned'       , transitionEnd, false);
            element.removeEventListener('webkitTransitionEnd', transitionEnd, false);
            element.removeEventListener('oTransitionEnd'     , transitionEnd, false);
            element.removeEventListener('MSTransitionEnd'    , transitionEnd, false);
          }
        }
        
        if ( elementTarget == 'auto' ) {
          element.style[ parameter ] = 'auto';
          elementTarget              = getComputedStyle(element)[ parameter ];
        }
        
        element.addEventListener('transitioned'       , transitionEnd);
        element.addEventListener('webkitTransitionEnd', transitionEnd);
        element.addEventListener('oTransitionEnd'     , transitionEnd);
        element.addEventListener('MSTransitionEnd'    , transitionEnd);

        element.style[ parameter ] = elementOrigin;
        element.offsetWidth; // force repaint
        element.style.transition   = [ parameter, duration + 'ms', 'ease-in-out' ].join(' ');
        element.style[ parameter ] = elementTarget;
      });
    });
    return object;
  };
  
})();