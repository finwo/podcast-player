(function() {
  if(typeof ff === 'undefined') {
    throw new Error('ff framework not loaded!');
  }
  
  /**
   * ff( selector ).on( eventName, callback )
   *     @param  {string}   eventName
   *     @param  {function} callback
   *     @return {array}    objectList
   *
   *     Attach an event listener to all objects in the list where the event identifies with the event name.
   *
   * ff( selector ).trigger( eventName )
   *     @param  {string} eventName
   *     @param  {object} callback [optional]
   *     @return {array}    objectList
   *
   *     Trigger an event by name on all objects in the list with optional data
   */
  
  ff.fn.on = function ( eventName, callback ) {
    this.each(function () {
      var entry = this;
      if ( entry.addEventListener ) {
        entry.addEventListener(eventName, callback);
      } else if ( entry.attachEvent ) {
        entry.attachEvent("on" + eventName, function () {
          return callback.call(entry, window.event);
        });
      }
    });
    return this;
  };
  
  ff.fn.trigger = function ( eventName ) {
    this.each(function () {
      var e = null;
      if ( document.createEventObject ) {
        e = document.createEventObject();
        this.fireEvent('on' + eventName, e);
      } else {
        e = document.createEvent('HTMLEvents');
        e.initEvent(eventName, true, true);
        this.dispatchEvent(e);
      }
    });
    return this;
  };
})();