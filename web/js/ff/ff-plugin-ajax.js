(function() {
  
  // Check dependencies
  if(typeof ff === 'undefined') {
    throw new Error('ff framework not loaded!');
  }
  if(typeof ff.promise === 'undefined') {
    throw new Error('ff.promise plugin not loaded!');
  }
  
  /**
   * ff.ajax( url )
   *     @param  {string}   eventName
   *     @param  {function} callback
   *     @return {object}   Promise
   */
  
  // XMLHTTP factories
  var factories = [
    function() { return new XMLHttpRequest(); },
    function() { return new ActiveXObject("Msxml3.XMLHTTP"); },
    function() { return new ActiveXObject("Msxml2.XMLHTTP.6.0"); },
    function() { return new ActiveXObject("Msxml2.XMLHTTP.3.0"); },
    function() { return new ActiveXObject("Msxml2.XMLHTTP"); },
    function() { return new ActiveXObject("Microsoft.XMLHTTP"); }
  ];
  function createXmlHttpObject() {
    var xmlhttp = false;
    factories.forEach(function(factory) {
      try {
        xmlhttp = xmlhttp || factory();
      } catch(e) {
        xmlhttp = false;
      }
    });
    return xmlhttp;
  }
  
  // http://stackoverflow.com/a/19448718
  function parseXml(xml, arrayTags) {
      var dom = null;
      if (window.DOMParser) {
          dom = (new DOMParser()).parseFromString(xml, "text/xml");
      } else if (window.ActiveXObject) {
          dom = new ActiveXObject('Microsoft.XMLDOM');
          dom.async = false;
          if (!dom.loadXML(xml)) {
              throw dom.parseError.reason + " " + dom.parseError.srcText;
          }
      } else {
          throw "cannot parse xml string!";
      }
  
      function isArray(o) {
          return Object.prototype.toString.apply(o) === '[object Array]';
      }
  
      function parseNode(xmlNode, result) {
          if(xmlNode.nodeName == "#text" && xmlNode.nodeValue.trim() == "") {
              return;
          }
  
          var jsonNode = {};
          var existing = result[xmlNode.nodeName];
          if(existing) {
              if(!isArray(existing)) {
                  result[xmlNode.nodeName] = [existing, jsonNode];
              } else {
                  result[xmlNode.nodeName].push(jsonNode);
              }
          } else {
              if(arrayTags && arrayTags.indexOf(xmlNode.nodeName) != -1) {
                  result[xmlNode.nodeName] = [jsonNode];
              } else {
                  result[xmlNode.nodeName] = jsonNode;
              }
          }
  
          if(xmlNode.attributes) {
              var length = xmlNode.attributes.length;
              for(var i = 0; i < length; i++) {
                  var attribute = xmlNode.attributes[i];
                  jsonNode[attribute.nodeName] = attribute.nodeValue;
              }
          }
  
          var length = xmlNode.childNodes.length;
          for(var i = 0; i < length; i++) {
              parseNode(xmlNode.childNodes[i], jsonNode);
          }
      }
  
      var result = {};
      if(dom.childNodes.length) {
          parseNode(dom.childNodes[0], result);
      }
  
      return result;
  }
  
  // The module we're building
  ff.ajax = function( url, postData ) {
    var p   = new ff.promise();
    var req = createXmlHttpObject();
    if (!req) {
      setTimeout(p.reject.bind(null,'Could net create xmlhttp object'), 1);
      return p;
    };
    
    // Default options
    var method = postData ? "POST" : "GET";
    req.open(method,url,true);
    req.setRequestHeader('User-Agent','XMLHTTP/1.0');
    if(postData) {
      req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    }
    req.onreadystatechange = function() {
      if (req.readyState != 4) return;
      if (req.status !== 200 && req.status != 304) return;
      var data = null;
      try { data = data || JSON.parse(req.responseText); } catch(e) {}
      try { data = data || parseXml(req.responseText);   } catch(e) {}
      try { data = data || req.responseText;             } catch(e) {}
      p.resolve(data);
    };
    if (req.readyState == 4) return p;
    req.send(postData);
    return p;
  };
  
})();