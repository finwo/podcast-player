(function() {
  if(typeof ff === 'undefined') {
    throw new Error('ff framework not loaded!');
  }
  
  function Promise () {
    this._queue = [];
  }
  Promise.prototype = {
    then: function (onResolve, onReject) {
      this._queue.push({ resolve: onResolve, reject: onReject });
      return this;
    },
    resolve: function (val) { this._complete('resolve', val); },
    reject: function (ex) { this._complete('reject', ex); },
    _complete: function (which, arg) {
      var entry, done = 0;
      if(!this._queue.length) return;
      while(!done) {
        entry = this._queue.shift();
        if ( entry[which] ) {
          entry[which](arg);
          return;
        }
      }
    }
  };
  
  ff.promise = Promise;
  
})();