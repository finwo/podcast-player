<?php

function startsWith( $haystack, $needle ) {
  return substr($haystack, 0, strlen($needle)) === $needle;
}

if(!function_exists('getallheaders')) {
  function getallheaders() {
    $headers = array();
    foreach($_SERVER as $name => $value) {
      if(startsWith($name,'HTTP_')) {
        $name = explode('_',$name);
        array_shift($name);
        $name = implode('-',array_map(function($el) {
          return ucfirst(strtolower($el));
        }, $name));
        $headers[$name] = $value;
      }
    }
    return $headers;
  }
}

function send( $url, $options = array() ) {
  $options = array_merge(array(
    'return'  => true,
    'method'  => 'GET',
    'headers' => array(),
    'body'    => false
  ), $options);
  $parts = parse_url($url);
  if(in_array(gettype($options['body']),array('array','object'))) {
    $options['body'] = http_build_query($options['body']);
    array_push($options['headers'],"Content-Type: application/x-www-form-urlencoded");
  }
  $fp = fsockopen($parts['host'],isset($parts['post'])?$parts['port']:80,$errno,$errstr,30);
  if(!fp) return false;
  $out  = strtoupper($options['method']) . " " . $parts['path'] . (isset($parts['query']) ? '?' . $parts['query'] : '') . " HTTP/1.0\r\n";
  $out .= "Host: " . $parts['host'] . "\r\n";
  if (count($options['headers'])) {
    $out .= implode("\r\n", $options['headers']) . "\r\n";
  }
  $out .= is_string($options['body']) ? sprintf("Content-Length: %s\r\n", strlen($options['body'])) : '';
  $out .= "Connection: Close\r\n\r\n";
  $out .= is_string($options['body']) ? $options['body'] : '';
  fwrite($fp, $out);
  $result = true;
  if ($options['return']) {
    $result = stream_get_contents($fp);
  }
  fclose($fp);
  return $result;
}

if(startsWith($_SERVER['REQUEST_URI'],'/proxy.php')) {
  $headers = getallheaders();
  unset($headers['Connection']);
  unset($headers['Cookie']);
  unset($headers['Host']);
  foreach($headers as $name => &$header) {
    $header = sprintf("%s: %s", $name, $header);
  }
  $rawResult = send($_GET['url'],array(
    'headers' => $headers,
  ));
  
  $result        = explode("\r\n\r\n", $rawResult, 2);
  $resultHeaders = explode("\r\n",array_shift($result));
  $resultBody    = array_shift($result);
  foreach($resultHeaders as $resultHeader) {
    header($resultHeader);
  }
  print($resultBody);
}