<?php

// Setup constants
define('DS'     , DIRECTORY_SEPARATOR);
define('APPROOT', rtrim(dirname(__DIR__),DS).DS);

// Init autoloader
require_once(implode(DS,array(__DIR__,'..','vendor','autoload.php')));

// Keeping the code short
use \Finwo\Framework\Bundle\AbstractBundle;
use \Finwo\Framework\Config\Config;
use \Klein\Klein;

// Initialize router
$router = new Klein();

// Initialize services
$services = array();
foreach( Config::get('services') as $name => $serviceClass ) {
  if(!class_exists($serviceClass)) continue;
  $service = new $serviceClass();
  if(!($service instanceof AbstractService)) continue;
  $services[$name] = $service;
}
Config::set('service', $services);

// Initialize registered bundles
$bundles = array();
foreach( Config::get('bundles') as $bundleName ) {
  $class = $bundleName . "\\" . @array_pop(explode("\\",$bundleName));
  if(!class_exists($class)) continue;
  $bundle = new $class($router);
  if (!($bundle instanceof AbstractBundle)) continue;
  array_push($bundles, new $class($router));
}
Config::set('bundles', $bundles);

// Kickstart the request
$router->dispatch();