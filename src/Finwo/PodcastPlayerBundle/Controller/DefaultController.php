<?php

namespace Finwo\PodcastPlayerBundle\Controller;

use \Finwo\Framework\Controller\AbstractController;

class DefaultController extends AbstractController
{
  
  /**
   * Default route
   * 
   * @method GET
   * @path /
   */
  public function defaultAction()
  {
    return array(
      "status" => 200,
      "result" => "Raw data response from controller is now supported",
    );
  }
}